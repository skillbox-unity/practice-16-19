using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.UI;

public class HouseCameraController : MonoBehaviour
{
    [SerializeField] private GameObject blackStripesCanvas;
    [SerializeField] private Text text;
    [SerializeField] private Animator blackStripesAnimator;
    [SerializeField] private PlayerInput playerInput;
    private bool isInputEnable;
    private string[] cutSceneTexts;
    private CinemachineVirtualCamera cinemachine;
    private float timeToChange;
    private float curentTimeToChange;
    private int maxIndexOfText;
    private int currentText;

    private void Awake()
    {
        cinemachine = GetComponent<CinemachineVirtualCamera>();
        isInputEnable = true;
        cutSceneTexts = new string[] { "Finally...", "I passed all tests", "Home Sweet Home" };
        timeToChange = 1.5f;
        curentTimeToChange = timeToChange;
        maxIndexOfText = cutSceneTexts.Length - 1;
        currentText = 0;
        text.text = cutSceneTexts[currentText].ToString();
    }

    private void Update()
    {
        if (!isInputEnable)
        {
            curentTimeToChange -= Time.deltaTime;
            if (curentTimeToChange <= 0)
            {
                if (currentText < maxIndexOfText)
                {
                    ChangeText();
                    curentTimeToChange = timeToChange;
                }
            }
        }
    }

    private void Start()
    {
        isInputEnable = false;
        playerInput.isInputEnable = isInputEnable;
    }

    private void ChangeText()
    {
        currentText++;
        text.text = cutSceneTexts[currentText].ToString();
    }


    public void HideBlackStripers()
    {
        blackStripesAnimator.SetTrigger("hideBlackStirpes");
    }

    public void DestroyCutScene()
    {
        isInputEnable = true;
        playerInput.isInputEnable = isInputEnable;
        cinemachine.m_Priority = 0;
        Destroy(blackStripesCanvas);
        Destroy(gameObject);
    }
}
