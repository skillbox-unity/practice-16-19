using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    [SerializeField] private ScoreController scoreController;
    private int scorePerCoin;

    private void Awake()
    {
        scorePerCoin = 100;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Character"))
        {
            scoreController.SetScore(scorePerCoin);
            Destroy(gameObject);
        }
    }
}
