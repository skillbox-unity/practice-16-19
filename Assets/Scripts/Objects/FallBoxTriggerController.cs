using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallBoxTriggerController : MonoBehaviour
{
    [SerializeField] private FallBoxController fallBoxController;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Character"))
        {
            fallBoxController.FallBox();
        }
    }
}
