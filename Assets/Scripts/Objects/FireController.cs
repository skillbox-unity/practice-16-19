using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireController : MonoBehaviour
{
    private int damage;

    private void Awake()
    {
        damage = 100;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Character"))
        {
            collision.gameObject.GetComponent<HPController>().TakeHeroDamage(damage);
        }
    }
}
