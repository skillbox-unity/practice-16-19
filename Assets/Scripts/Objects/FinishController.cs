using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishController : MonoBehaviour
{
    [SerializeField] private ScoreController scoreController;
    [SerializeField] private GUIController guiController;

    private int scorePerFinish;

    private void Awake()
    {
        scorePerFinish = 2000;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Character"))
        {
            scoreController.SetScore(scorePerFinish);
            guiController.ShowWinScreenCanvas();
        }
    }
}
