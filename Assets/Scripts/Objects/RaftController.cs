using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaftController : MonoBehaviour
{
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Character"))
        {
            float raftX = gameObject.GetComponent<Rigidbody2D>().velocity.x;
            float characterY = collision.gameObject.GetComponent<Rigidbody2D>().velocity.y;
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(raftX, characterY);
        }
    }
}
