using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallBoxController : MonoBehaviour
{
    private int damage;

    private void Awake()
    {
        damage = 100;
    }

    public void FallBox()
    {
        GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Character"))
            collision.gameObject.GetComponent<HPController>().TakeHeroDamage(damage);
    }
}
