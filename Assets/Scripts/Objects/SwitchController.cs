using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchController : MonoBehaviour
{
    [SerializeField] private GameObject[] fallBoxes;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Strike"))
        {
            foreach (GameObject gameObj in fallBoxes)
            {
                gameObj.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            }
        }
    }
}
