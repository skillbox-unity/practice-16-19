using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPlatformController : MonoBehaviour
{
    [SerializeField] private float force;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Character"))
        {
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * force, ForceMode2D.Impulse);
        }
    }
}
