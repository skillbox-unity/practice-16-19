using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderPlatformController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Character"))
        {
            GetComponent<SliderJoint2D>().useMotor = true;
        }
    }
}
