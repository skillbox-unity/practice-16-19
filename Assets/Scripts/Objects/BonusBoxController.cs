using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusBoxController : MonoBehaviour
{
    [SerializeField] private ScoreController scoreController;
    private int scorePerBox;

    private void Awake()
    {
        scorePerBox = 5000;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Strike"))
        {
            scoreController.SetScore(scorePerBox);
            Destroy(gameObject);
        }
    }
}
