using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttackController : MonoBehaviour
{
    [SerializeField] private GameObject crystal;
    [SerializeField] private GameObject lineOfFire;
    [SerializeField] private GameObject thunder;
    [SerializeField] private Transform heroTransform;

    public void Attack1()
    {
        int dir;
        if (heroTransform.position.x - transform.position.x < 0)
            dir = -1;
        else
            dir = 1;
        Vector3 currentPos = new Vector3(transform.position.x + dir, transform.position.y + 0.2f, transform.position.z);
        GameObject currentLineOfFire = Instantiate(lineOfFire, currentPos, Quaternion.identity) as GameObject;
        if (dir < 0)
            currentLineOfFire.GetComponent<SpriteRenderer>().flipX = false;
        else
            currentLineOfFire.GetComponent<SpriteRenderer>().flipX = true;
    }

    public void Attack2()
    {
        Instantiate(crystal, heroTransform.position, Quaternion.identity);
    }

    public void Attack3()
    {
        Instantiate(thunder, heroTransform.position, Quaternion.identity);
    }
}
