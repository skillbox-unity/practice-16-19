using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalController : MonoBehaviour
{
    private float timeToBurst;
    private float currentTimeToBurst;
    private bool isCrystalAppeared;
    [SerializeField] private float damage;
    private Animator animator;

    private void Awake()
    {
        timeToBurst = -1;
        animator = GetComponent<Animator>();
        isCrystalAppeared = false;
    }
   
    private void Update()
    {
        if (timeToBurst != -1)
        {
            currentTimeToBurst -= Time.deltaTime;
            if (currentTimeToBurst <= 0)
            {
                timeToBurst = -1;
                BurstDamage();
                animator.SetTrigger("isBurst");
            }
        }
    }

    private void BurstDamage()
    {
        GameObject character = GameObject.FindGameObjectWithTag("Character");
        character.GetComponent<HPController>().TakeHeroDamage(damage);
    }

    public void DestroyCrystal()
    {

        Destroy(gameObject);
    }

    public void OnCrystalAppeared()
    {
        if (!isCrystalAppeared)
        {
            timeToBurst = 4.0f;
            currentTimeToBurst = timeToBurst;
            isCrystalAppeared = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Strike"))
        {
            Destroy(gameObject);
        }
    }
}
