using UnityEngine;

public class BossController : MonoBehaviour
{
    private float timeToAttack;
    private float currentTimeToAttack;
    private int countOfAttack;
    private int currentTypeOfAttack;
    private BossAnimatorController bossAnimationController;
    private BossMovement bossMovement;
    private BossAttackController bossAttackController;

    private void Awake()
    {
        bossAnimationController = GetComponent<BossAnimatorController>();
        bossMovement = GetComponent<BossMovement>();
        bossAttackController = GetComponent<BossAttackController>();
        GenerateTimeToAttack();
        GeneratePhase();
    }

    void Update()
    {
        currentTimeToAttack -= Time.deltaTime;
        if (currentTimeToAttack <= 0)
            BossAttack();
    }

    private void BossAttack()
    {
        currentTypeOfAttack = Random.Range(1, 4);
        bossAnimationController.BossAttackAnimationStart(currentTypeOfAttack);

        GenerateTimeToAttack();

        countOfAttack--;
        if (countOfAttack == 0)
            GeneratePhase();
    }

    private void GeneratePhase()
    {
        countOfAttack = Random.Range(3, 6);
        bossMovement.GeneratePosition();
    }

    private void GenerateTimeToAttack()
    {
        timeToAttack = Random.Range(1, 4);
        currentTimeToAttack = timeToAttack;
    }
}
