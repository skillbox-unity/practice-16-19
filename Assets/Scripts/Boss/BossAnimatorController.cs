﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class BossAnimatorController : MonoBehaviour
{
    [SerializeField] private GUIController guiController;
    private Animator aniamtor;

    private void Awake()
    {
        aniamtor = GetComponent<Animator>();
    }

    public void BossAttackAnimationStart(int currentTypeOfAttack)
    {
        aniamtor.SetTrigger($"isAttack{currentTypeOfAttack}");
    }

    public void DestroyBoss()
    {
        Destroy(gameObject);
        guiController.ShowWinScreenCanvas();
    }
}
