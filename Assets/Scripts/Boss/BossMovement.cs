using UnityEngine;

public class BossMovement : MonoBehaviour
{
    private Vector2 currentPosition;
    [SerializeField] private float speed;

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, currentPosition, speed * Time.deltaTime);
        if (transform.position.x > 0)
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        else
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
    }

    public void GeneratePosition()
    {
        int pos = Random.Range(1, 5);

        switch (pos)
        {
            case 1:
                currentPosition = new Vector2(-1.5f, -1.5f);
                break;
            case 2:
                currentPosition = new Vector2(1.5f, -1.5f);
                break;
            case 3:
                currentPosition = new Vector2(-1.5f, 0);
                break;
            case 4:
                currentPosition = new Vector2(1.5f, 0);
                break;
            default:
                currentPosition = Vector2.zero;
                break;
        }
    }
}
