using UnityEngine;

[RequireComponent(typeof(CapsuleCollider2D))]
public class HPController : MonoBehaviour
{
    [SerializeField] private float maxHP;
    private EnemyAnimatorController enemyAnimatorController;
    private AnimatorController animatorController;
    private float currentHP;
    [SerializeField] private HPBarController hpBarController;
    private bool isAlreadyDead;
    [SerializeField] GUIController guiController;
    [SerializeField] ScoreController scoreController;
    private int scorePerKill;
    private const int commonEnemyHP = 100;

    private void Awake()
    {
        enemyAnimatorController = GetComponent<EnemyAnimatorController>();
        animatorController = GetComponent<AnimatorController>();
        currentHP = maxHP;
        hpBarController.SetMaxHP(maxHP);
        isAlreadyDead = false;
        SetScorePerKill();
    }

    public void TakeEnemyDamage(float damage)
    {
        TakeDamage(damage);
        if (!CheckIsAlive()) {
            if (!isAlreadyDead)
            {
                DestroyCollider();
                isAlreadyDead = true;
                enemyAnimatorController.Dead();
                scoreController.SetScore(scorePerKill);
            }
        }
        else
        {
            enemyAnimatorController.TakeHit();
        }
    }

    public void TakeHeroDamage(float damage)
    {
        TakeDamage(damage);
        if (!CheckIsAlive())
        {
            if (!isAlreadyDead)
            {
                DestroyCollider();
                isAlreadyDead = true;
                animatorController.Dead();
            }
        }
        else
        {
            animatorController.TakeHit();
        }
    }

    private void TakeDamage(float damage)
    {
        currentHP -= damage;
        hpBarController.SetCurrentHP(currentHP);
    }

    public bool CheckIsAlive()
    {
        bool isAlive;
        if (currentHP <= 0)
            isAlive = false;
        else
            isAlive = true;
        return isAlive;
    }

    private void SetScorePerKill()
    {
        if (maxHP > commonEnemyHP)
            scorePerKill = 10000;
        else
            scorePerKill = 1000;
    }

    private void DestroyCollider()
    {
        gameObject.tag = "Dead";
        foreach (Transform child in gameObject.transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void PlayerDead()
    {
        guiController.ShowLoseScreenCanvas();
    }
}
