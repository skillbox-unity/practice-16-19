public class GlobalVars
{
    #region Movement

    public const string HORIZONTAL_AXIS = "Horizontal";
    public const string VERTICAL_AXIS = "Vertical";
    public const string JUMP = "Jump";

    #endregion

    public const string FIRE_1 = "Fire1";
    public const float TOLERANCE = 0.01f;
}
