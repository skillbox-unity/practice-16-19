using UnityEngine;

[RequireComponent(typeof(AnimatorController))]
public class StrikeController : MonoBehaviour
{
    [SerializeField] private Transform strikePoint;
    [SerializeField] private GameObject strike;

    private AnimatorController animatorController;
    private bool isStrike;
    private int direction;
    private Vector3 currentPosition;
    private GameObject currentStrike;
    [HideInInspector] public bool isFlip;

    private void Awake()
    {
        animatorController = GetComponent<AnimatorController>();
        isStrike = false;
    }

    private void Update()
    {
        CheckDirection();
        SetStrikePosition();
    }

    public void StartStrike()
    {
        if (!isStrike)
        {
            animatorController.StartStrikeAnimation();
        }

        isStrike = true;
    }

    public void Strike()
    {
        currentPosition = new Vector3(strikePoint.localPosition.x * direction, strikePoint.localPosition.y, strikePoint.localPosition.z);
        currentStrike = Instantiate(strike, strikePoint.position, Quaternion.identity, transform) as GameObject;
        currentStrike.transform.localPosition = currentPosition;
    }

    public void DestroyStrike()
    {
        isStrike = false;
        Destroy(currentStrike);
    }

    private void CheckDirection()
    {
        if (!isFlip)
            direction = 1;
        else
            direction = -1;
    }

    private void SetStrikePosition()
    {
        if (currentStrike != null)
        {
            currentPosition = new Vector3(strikePoint.localPosition.x * direction, strikePoint.localPosition.y, strikePoint.localPosition.z);
            currentStrike.transform.localPosition = currentPosition;
        }
    }
}
