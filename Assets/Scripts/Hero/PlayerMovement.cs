using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float jumpSpeed;
    [SerializeField] private float speed;
    [SerializeField] private AnimationCurve accelerationCurve;

    public LayerMask platformLayerMask;

    [HideInInspector] public Rigidbody2D playerRigitbody2D;
    [HideInInspector] public bool isGrounded = false;
    [HideInInspector] public float animationSpeed;
    [HideInInspector] public bool isMoving;

    void Awake()
    {
        isMoving = false;
        playerRigitbody2D = GetComponent<Rigidbody2D>();
        animationSpeed = 1;
    }

    public void Move(float horizontalDirection, bool isJumpPressed)
    {
        if (isJumpPressed)
            Jump();
        if (Mathf.Abs(horizontalDirection) > GlobalVars.TOLERANCE)
        {
            isMoving = true;
            HorizontalMovement(horizontalDirection);
            animationSpeed = accelerationCurve.Evaluate(horizontalDirection);
        }
        else
        {
            isMoving = false;
            animationSpeed = 1;
        }
            
    }

    private void Jump()
    {
        if (isGrounded)
            playerRigitbody2D.velocity = new Vector2(playerRigitbody2D.velocity.x, jumpSpeed);
    }

    private void HorizontalMovement(float horizontalDirection)
    {
        playerRigitbody2D.velocity = new Vector2(accelerationCurve.Evaluate(horizontalDirection) * speed, playerRigitbody2D.velocity.y);
        if (horizontalDirection < 0)
            GetComponent<SpriteRenderer>().flipX = true;
        else
            GetComponent<SpriteRenderer>().flipX = false;

    }

}

