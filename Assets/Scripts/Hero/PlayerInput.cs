using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(StrikeController))]
[RequireComponent(typeof(HPController))]
public class PlayerInput : MonoBehaviour
{
    private PlayerMovement playerMovement;
    private StrikeController strikeController;
    private HPController hpController;
    private SpriteRenderer spriteRenderer;
    private bool isFlip;
    [HideInInspector] public bool isInputEnable;

    void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();
        strikeController = GetComponent<StrikeController>();
        hpController = GetComponent<HPController>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        isInputEnable = true;
    }

    private void Update()
    {
        if (hpController.CheckIsAlive() && isInputEnable) {
            float horizontalDirection = Input.GetAxis(GlobalVars.HORIZONTAL_AXIS);
            bool isJumpPressed = Input.GetButtonDown(GlobalVars.JUMP);
            playerMovement.Move(horizontalDirection, isJumpPressed);
            isFlip = spriteRenderer.flipX;
            strikeController.isFlip = isFlip;
            if (Input.GetButtonDown(GlobalVars.FIRE_1))
            {
                strikeController.StartStrike();
            }
        }
        else
        {
            if (!isInputEnable)
                playerMovement.Move(0, false);
        }
    }
}
