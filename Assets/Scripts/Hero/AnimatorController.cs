using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(Animator))]

public class AnimatorController : MonoBehaviour
{
    private PlayerMovement playerMovement;
    private Animator playerAnimator;
    private Rigidbody2D playerRigidbody2D;

    void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();
        playerAnimator = GetComponent<Animator>();
        playerRigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Mathf.Abs(playerMovement.animationSpeed) > 0.3f)
            playerAnimator.SetFloat("runningSpeed", Mathf.Abs(playerMovement.animationSpeed));
    }

    void FixedUpdate()
    {
        CheckGrounded();
        CheckRunning();
    }

    private void CheckGrounded()
    {
        float toleranceJumpVelocity = 0.5f;
        float velocityY = Mathf.Abs(playerRigidbody2D.velocity.y);
        bool isOverlapCircle = Physics2D.OverlapCircle(transform.position, 0.03f, playerMovement.platformLayerMask);
        playerMovement.isGrounded = isOverlapCircle;
        if (isOverlapCircle && velocityY < toleranceJumpVelocity)
            playerAnimator.SetInteger("jump", 0);
        else
            playerAnimator.SetInteger("jump", 1);
    }

    private void CheckRunning()
    {
        playerAnimator.SetBool("isRunning", playerMovement.isMoving);
    }

    public void StartStrikeAnimation()
    {
        playerAnimator.SetTrigger("isAttack1");
    }

    public void Dead()
    {
        playerAnimator.SetTrigger("isDead");
    }

    public void TakeHit()
    {
        playerAnimator.SetTrigger("isTakeHit");
    }
}
