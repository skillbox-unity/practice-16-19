using UnityEngine;

public class RotarePlatform : MonoBehaviour
{
    void Update()
    {
        HingeJoint2D hinge = GetComponent<HingeJoint2D>();
        JointMotor2D motor = hinge.motor;
        if (hinge.jointAngle <= -45 && motor.motorSpeed < 0)
        {
            motor.motorSpeed *= -1;
        }
        else
        {
            if (hinge.jointAngle >= 45 && motor.motorSpeed > 0)
            {
                motor.motorSpeed *= -1;
            }
        }
        hinge.motor = motor;
    }
}
