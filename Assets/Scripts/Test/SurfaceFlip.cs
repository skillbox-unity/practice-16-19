using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceFlip : MonoBehaviour
{
    [SerializeField] private GameObject boxObject;
    private Transform boxTransform;
    private BoxCollider2D boxCollider;
    private BoxCollider2D platformCollider;
    private SurfaceEffector2D platformSurfaceEffector2D;

    private void Start()
    {
        boxTransform = boxObject.GetComponent<Transform>();
        boxCollider = boxObject.GetComponent<BoxCollider2D>();
        platformCollider = GetComponent<BoxCollider2D>();
        platformSurfaceEffector2D = GetComponent<SurfaceEffector2D>();
    }

    void Update()
    {
        float leftBoxPosition = boxTransform.position.x - boxCollider.size.x / 2;
        float rightBoxPosition = boxTransform.position.x + boxCollider.size.x / 2;
        float leftPlatformPosition = transform.position.x - platformCollider.size.x / 2;
        float rightPlatformPosition = transform.position.x + platformCollider.size.x / 2;
        float surfaceSpeed = platformSurfaceEffector2D.speed;

        if (rightBoxPosition >= rightPlatformPosition &&  surfaceSpeed> 0)
        {
            platformSurfaceEffector2D.speed *= -1;
        }
        else
        {
            if (leftBoxPosition <= leftPlatformPosition && surfaceSpeed < 0)
            {
                platformSurfaceEffector2D.speed *= -1;
            }
        }
    }
}
