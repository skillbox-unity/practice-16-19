using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeScript : MonoBehaviour
{
    [SerializeField] private GameObject explode;

    private GameObject explodeObject;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        explodeObject = Instantiate(explode, transform.position, transform.rotation, transform);
        StartCoroutine(StartExplode());
    }

    private IEnumerator StartExplode()
    {
        yield return new WaitForSeconds(0.1f);
        Destroy(explodeObject);
    }
}
