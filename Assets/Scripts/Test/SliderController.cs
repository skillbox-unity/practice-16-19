using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderController : MonoBehaviour
{
    void Update()
    {
        SliderJoint2D slider = GetComponent<SliderJoint2D>();
        JointMotor2D motor = slider.motor;
        Debug.Log(slider.jointTranslation);
        if (slider.jointTranslation >= slider.limits.max && motor.motorSpeed > 0)
        {
            motor.motorSpeed *= -1;
        }
        else
        {
            if (slider.jointTranslation <= slider.limits.min && motor.motorSpeed < 0)
            {
                motor.motorSpeed *= -1;
            }
        }
        slider.motor = motor;
    }
}
