using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChoseLevelButtonController : MonoBehaviour
{
    public void OnChooseLevel(int level)
    {
        SceneManager.LoadScene(level);
    }
}
