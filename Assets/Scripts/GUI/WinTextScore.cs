using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinTextScore : MonoBehaviour
{
    [SerializeField] private ScoreController scoreController;

    private void Awake()
    {
        Text text = GetComponent<Text>();
        string winScore = scoreController.GetScore().ToString();
        text.text = $"Score: {winScore}";
    }
}
