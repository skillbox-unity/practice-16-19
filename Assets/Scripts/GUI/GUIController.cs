using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIController : MonoBehaviour
{
    [SerializeField] private GameObject loseScreenCanvas;
    [SerializeField] private GameObject winCanvas;
    [SerializeField] private PlayerInput playerInput;

    private void Awake()
    {
        if(loseScreenCanvas != null)
            loseScreenCanvas.SetActive(false);
        if(winCanvas != null)
            winCanvas.SetActive(false);
    }

    public void ShowLoseScreenCanvas()
    {
        playerInput.isInputEnable = false;
        loseScreenCanvas.SetActive(true);
    }

    public void ShowWinScreenCanvas()
    {
        playerInput.isInputEnable = false;
        winCanvas.SetActive(true);
    }
}
