using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    private Text scoreText;
    private int currentScore;

    private void Awake()
    {
        scoreText = GetComponent<Text>();
        currentScore = 0;
        SetScore(currentScore);
    }

    public void SetScore(int addedScore)
    {
        currentScore += addedScore;
        scoreText.text = $"Score: {currentScore}";
    }

    public int GetScore()
    {
        return currentScore;
    }
}
