using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamageDealer : MonoBehaviour
{
    [SerializeField] private float damage;
    private Animator chargeAnimator;
    private float stopDelay;

    private void Awake()
    {
        stopDelay = 0.05f;
        chargeAnimator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Character"))
        {
            collision.gameObject.GetComponent<HPController>().TakeHeroDamage(damage);
            chargeAnimator.SetTrigger("isExplode");
            StartCoroutine(StopChargeTimer(stopDelay));
        }
        if (collision.CompareTag("Box") || collision.CompareTag("Platform") || collision.CompareTag("Charge"))
        {
            chargeAnimator.SetTrigger("isExplode");
            StartCoroutine(StopChargeTimer(stopDelay));
        }
    }

    public void DestroyCharge()
    {
        Destroy(gameObject);
    }

    private IEnumerator StopChargeTimer(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
    }
}
