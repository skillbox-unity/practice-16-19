using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStrikeController : MonoBehaviour
{
    [SerializeField] private GameObject charge;
    [SerializeField] private float speedCharge;
    [SerializeField] private Transform pointCharge;
    private HPController hpcontroller;

    private EnemyAnimatorController enemyAnimatorController;

    private void Awake()
    {
        enemyAnimatorController = GetComponent<EnemyAnimatorController>();
        hpcontroller = GetComponent<HPController>();
    }

    public void EnemyShot()
    {
        if (hpcontroller.CheckIsAlive()) {
            enemyAnimatorController.StartShootAnimation();

            float direction;
            bool isFlip = GetComponent<SpriteRenderer>().flipX;
            if (!isFlip)
                direction = -1;
            else
                direction = 1;

            Vector3 currentPosition = new Vector3(pointCharge.localPosition.x * direction, pointCharge.localPosition.y, pointCharge.localPosition.z);
            GameObject currentCharge = Instantiate(charge, pointCharge.position, Quaternion.identity, transform) as GameObject;
            currentCharge.transform.localPosition = currentPosition;

            SpriteRenderer currentSpriteRenderer = currentCharge.GetComponent<SpriteRenderer>();
            currentSpriteRenderer.flipX = isFlip;

            Rigidbody2D currentChargeRigidbody2D = currentCharge.GetComponent<Rigidbody2D>();
            currentChargeRigidbody2D.velocity = new Vector2(speedCharge * -direction, currentChargeRigidbody2D.velocity.y);
        }
    }
}
