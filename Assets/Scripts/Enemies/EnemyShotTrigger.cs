using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShotTrigger : MonoBehaviour
{
    [SerializeField] private EnemyStrikeController enemyStrikeController;
    [SerializeField] private float maxShootTimer;
    private float currentShootTimer;
    bool isShooting;

    private void Awake()
    {
        isShooting = false;
        currentShootTimer = 0;
    }

    private void Update()
    {
        if (currentShootTimer <= 0)
        {
            isShooting = false;
        }
        else
        {
            currentShootTimer -= Time.deltaTime;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Character") && !isShooting)
        {
            enemyStrikeController.EnemyShot();
            isShooting = true;
            currentShootTimer = maxShootTimer;
        }
    }
}
