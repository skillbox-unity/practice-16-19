using UnityEngine;

[RequireComponent(typeof(Animator))]
public class EnemyAnimatorController : MonoBehaviour
{
    private Animator enemyAnimator;

    void Awake()
    {
        enemyAnimator = GetComponent<Animator>();
    }

    public void StartShootAnimation()
    {
        enemyAnimator.SetTrigger("isAttack1");
    }

    public void Dead()
    {
        enemyAnimator.SetTrigger("isDead");
    }

    public void TakeHit()
    {
        enemyAnimator.SetTrigger("isTakeHit");
    }
}
