using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClashHero : MonoBehaviour
{
    [SerializeField] private float damage;
    [SerializeField] private float positionOffset;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Character"))
        {
            collision.gameObject.GetComponent<HPController>().TakeHeroDamage(damage);
            collision.transform.position = new Vector3(collision.transform.position.x + positionOffset * (collision.transform.position - transform.position).normalized.x, collision.transform.position.y, collision.transform.position.z);
        }
    }
}
